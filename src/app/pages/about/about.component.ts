import { Component, OnInit } from '@angular/core';
import { InfoEquipo } from 'src/app/interfaces/info-pagina.inteface';
import { InfoPaginaService } from 'src/app/services/info-pagina.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor( public _servicio: InfoPaginaService ) { }

  ngOnInit(): void {
  }

}
