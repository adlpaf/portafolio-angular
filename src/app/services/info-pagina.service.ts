import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { InfoEquipo, InfoPagina } from '../interfaces/info-pagina.inteface';

@Injectable({
  providedIn: 'root'
})
export class InfoPaginaService {

  info: InfoPagina = {};
  cargada = false;
  equipo: InfoEquipo[] = [];

  constructor( private http: HttpClient ) {
    this.cargarinfo();
    this.cargarEquipo();
  }

  private cargarinfo() {
    this.http.get<InfoPagina>('assets/data/data-pagina.json')
    .subscribe(resp => {
      this.cargada = true;
      this.info = resp;
    });
  }

  private cargarEquipo() {
    this.http.get<InfoEquipo[]>('https://portafolio-angular-3995f-default-rtdb.firebaseio.com/equipo.json')
    .subscribe(resp => {
      this.equipo = resp;
    });
  }
}
