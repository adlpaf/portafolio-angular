import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Producto, ProductosIDX } from '../interfaces/info-productos.interface';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  productos: ProductosIDX[] = [];
  productosFiltrados: ProductosIDX[] = [];
  cargando = true;

  constructor( private http: HttpClient ) {
    this.cargarProductos();
  }

  private cargarProductos() {
    return new Promise<void>( (resolve, reject ) => {
      this.http.get<ProductosIDX[]>("https://portafolio-angular-3995f-default-rtdb.firebaseio.com/productos_idx.json")
      .subscribe( resp => {
        this.cargando = false;
        this.productos = resp;
        resolve();
      });
    });
  }

  getProducto(id: string) {
    return this.http.get<Producto>(`https://portafolio-angular-3995f-default-rtdb.firebaseio.com/productos/${id}.json`);
  }

  buscarProducto( termino: string ) {
    if (this.productos.length === 0) {
      this.cargarProductos().then( () => {
        this.filtrarProductos(termino);
      });
    } else {
      this.filtrarProductos(termino);
    }
  }

  private filtrarProductos(termino: string) {
    this.productosFiltrados = [];
    termino = termino.toLowerCase();
    this.productos.forEach(item => {
      if (
        item.categoria!.toLocaleLowerCase().indexOf(termino) >= 0 ||
        item.titulo!.toLocaleLowerCase().indexOf(termino) >= 0
      ) {
        this.productosFiltrados.push(item);
      }
    });
  }

}
